import {terser} from 'rollup-plugin-terser';

export default {
	input: 'src/scorm.js',
	output: [
		{
			file: 'build/scormv1.2-driver.js',
			format: 'umd',
			name: 'scorm'
		},
		{
			file: 'build/scormv1.2-driver.min.js',
			format: 'umd',
			name: 'scorm',
			plugins: [terser()]
		},
		{
			file: 'build/scormv1.2-driver.common.js',
			format: 'cjs'
		},
		{
			file: 'build/scormv1.2-driver.esm.js',
			format: 'es'
		}
	]
};