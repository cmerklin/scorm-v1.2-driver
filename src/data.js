/**
 * Data Class
 *
 * @author 		Christian Merklinger
 * @date 		2020/04/27
 * @version 	1.0.0
 */

// ---------------- Imports ----------------
import Utils from "./utils.js";
import Debug from "./debug.js";
import Connection from "./connection.js";

//------------------------ Data Class ------------------------

export default class Data {
	constructor(connection, debug) {
		this._connection = connection;
		this._debug = debug;
	}

	set connection(object) {
		if (object instanceof Connection || object === null) {
			this._connection = object;
		}
	}

	get connection() {
		return this._connection;
	}

	set debug(object) {
		if (object instanceof Debug || object === null) {
			this._debug = object;
		}
	}

	get debug() {
		return this._debug;
	}

	/** Fetchs information from the LMS API Adapter
	 * @param parameter 		String which contains the SCORM data model element
	 *
	 * @return 					String value of the specified data model element, or empty string if an error occured
	 */
	getValue(parameter) {
		let response = "";
		let errorCode = null;
		
		try {
			response = this._connection.getAPI().LMSGetValue(parameter);
			errorCode = this._debug.getLastErrorCode();

			if (errorCode !== 0) {
				response = "";

				let errorString = this._debug.getErrorString(errorCode);

				this._debug.log(
					"error",
					{
						type: "LMS",
						errorCode: errorCode,
						errorString: errorString
					}
				);
			}

		} catch (error) {
			this._debug.log(
				"error",
				{
					type: "Wrapper",
					msg: "An error occured while fetching data from the LMS API Adapter."
				}
			);
			this._debug.log(
				"error",
				{
					type: "Wrapper",
					msg: error
				}
			);

		}

		return response;

	}

	/** Pushs data to the LMS API Adapter
	 * @param parameter 		String which contains the SCORM data model element
	 * @param value 			String/Number which contains the value to assign to the SCORM data model element
	 * 
	 * @return 					True if value successfull pushed, false if not
	 */
	setValue(parameter, value) {
		let response = false;
		let errorCode = null;

		try {
			response = Utils.convertStringToBoolean( this._connection.getAPI().LMSSetValue(parameter, value) );
			errorCode = this._debug.getLastErrorCode();

			if (errorCode !== 0) {
				response = false;

				let errorString = this._debug.getErrorString(errorCode);

				this._debug.log(
					"error",
					{
						type: "LMS",
						errorCode: errorCode,
						errorString: errorString
					}
				);
			}

		} catch (error) {
			this._debug.log(
				"error",
				{
					type: "Wrapper",
					msg: "An error occured while fetching data from the LMS API Adapter."
				}
			);
			this._debug.log(
				"error",
				{
					type: "Wrapper",
					msg: error
				}
			);

		}

		return response;

	}

	/** Forces LMS API Adapter to push data to the LMS
	 * 
	 * @return 					True if successfull commited, false if not
	 */
	commit() {
		let response = false;
		let errorCode = null;

		try {
			response = Utils.convertStringToBoolean( this._connection.getAPI().LMSCommit("") );
			errorCode = this._debug.getLastErrorCode();

			if (errorCode !== 0) {
				response = false;

				let errorString = this._debug.getErrorString(errorCode);

				this._debug.log(
					"error",
					{
						type: "LMS",
						errorCode: errorCode,
						errorString: errorString
					}
				);
			}

		} catch (error) {
			this._debug.log(
				"error",
				{
					type: "Wrapper",
					msg: "An error occured while forcing LMS API Adapter to commit data to the LMS."
				}
			);
			this._debug.log(
				"error",
				{
					type: "Wrapper",
					msg: error
				}
			);

		}

		return response;

	}
	
}