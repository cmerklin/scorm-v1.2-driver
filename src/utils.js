/**
 * Utils Class
 *
 * @author 		Christian Merklinger
 * @date 		2020/04/27
 * @version 	1.0.0
 */

// ---------------- Imports ----------------


//------------------------ Utils Class ------------------------

export default class Utils {
	constructor() {

	}

	/** Converts an String value to an Boolean value
	 * @param value 			Value which should converted to an String
	 * 
	 * @return 					Boolean value of the passed in value
	 */
	static convertStringToBoolean(value) {
		switch (typeof value) {
			case "object":
			case "string":
				return (/(true|1)/i).test(value);
			case "number":
				return !!value;
			case "boolean":
				return value;
			case "undefined":
				return null;
			default:
				return false;
		}
	}

}