/**
 * SCORM Class
 *
 * @author 		Christian Merklinger
 * @date 		2020/05/10
 * @version 	1.0.0
 */

// ---------------- Imports ----------------
import Connection from "./connection.js";
import Data from "./data.js";
import Debug from "./debug.js";

//------------------------ SCORM Class ------------------------

export default class Scorm {
	constructor(debugLevel = 0) {
		this._connection = new Connection();
		this._data = null;
		this._debug = null;
		this._debugLevel = debugLevel;

		this._isConnectionActive = false;

		console.info("SCORM Wrapper initialized");
	}

	set isConnectionActive(status) {
		if (typeof status === "boolean") {
			this._isConnectionActive = status;
		}
	}

	get isConnectionActive() {
		return this._isConnectionActive;
	}

	set connection(object) {
		if (object instanceof Connection || object === null) {
			this._connection = object;
		}
	}

	get connection() {
		return this._connection;
	}

	set data(object) {
		if (object instanceof Data || object === null) {
			this._data = object;
		}
	}

	get data() {
		return this._data;
	}

	set debug(object) {
		if (object instanceof Debug || object === null) {
			this._debug = object;
		}
	}

	get debug() {
		return this._debug;
	}

	set debugLevel(debugLevel) {
		if (typeof debugLevel === "number") {
			this._debugLevel = debugLevel;
		}
	}

	get debugLevel() {
		return this._debugLevel;
	}

	/** Establishes the connection to the LMS
	 * 
	 * @return 					True if connection is established, false if not
	 */
	initialize() {
		try {
			// Check connection is not already active
			if (!this._isConnectionActive) {
				this._isConnectionActive = this._connection.initialize();

				if (this._isConnectionActive) {
					this._debug = new Debug(this._connection, this._debugLevel);
					this._data = new Data(this._connection, this._debug);

					this._debug.log(
						"info",
						{
							type: "Wrapper",
							msg: "Connection with LMS API Adapter established."
						}
					);

				} else {
					this._debug = new Debug(null, this._debugLevel);
					this._debug.log(
						"error",
						{
							type: "Wrapper",
							msg: "Connection with LMS API Adapter not established, probably API Adapter not found."
						}
					);

				}

			} else {
				this._debug.log(
					"warning",
					{
						type: "Wrapper",
						msg: "Connection with LMS API Adapter already established."
					}
				);
			}

		} catch (error) {
			this._debug = new Debug(null, this._debugLevel);
			this._debug.log(
				"error",
				{
					type: "Wrapper",
					msg: "An error occured while initializing the connection to the LMS API Adapter."
				}
			);
			this._debug.log(
				"error",
				{
					type: "Wrapper",
					msg: error
				}
			);

		}

		return this._isConnectionActive;
	}

	/** Terminates the connection with the LMS API Adapter
	 ** Before terminating connection probably cache date in API Adapter gets commited to the LMS
	 * 
	 * @return 					True if connection successfull terminated, false if not
	 */
	finish() {
		let response = false;
		
		try {

			// Check connection is active
			if (this._isConnectionActive) {
				let responseCommit = this._data.commit();

				if (responseCommit) {
					response = this._connection.terminate();

					if (response) {
						this._isConnectionActive = false;
						this._connection = null;
						this._debug.log(
							"info",
							{
								type: "Wrapper",
								msg: "Connection with LMS API Adapter terminated."
							}
						);
					} else {
						this._debug.log(
							"warning",
							{
								type: "Wrapper",
								msg: "Could not terminate connection to the LMS API Adapter because LMS API Adapter rejected termination."
							}
						);
					}

				} else {
					this._debug.log(
						"warning",
						{
							type: "Wrapper",
							msg: "Could not terminate connection to the LMS API Adapter because persisting data to the LMS is not possible."
						}
					);
				}

			} else {
				this._debug = new Debug(null, this._debugLevel);
				this._debug.log(
					"warning",
					{
						type: "Wrapper",
						msg: "Could not terminate connection to the LMS API Adapter because connection is not established."
					}
				);

			}

		} catch (error) {
			this._debug = new Debug(null, this._debugLevel);
			this._debug.log(
				"error",
				{
					type: "Wrapper",
					msg: "An error occured while terminating the connection to the LMS API Adapter."
				}
			);
			this._debug.log(
				"error",
				{
					type: "Wrapper",
					msg: error
				}
			);

		}

		return response;
	}

	/** Fetchs information from the LMS API Adapter
	 * @param parameter 		String which contains the SCORM data model element
	 * 
	 * @return 					String value of the specified data model element, or empty string if an error occured
	 */
	getValue(parameter) {
		let response = "";

		// Check connection is active
		if (this._isConnectionActive) {
			// Check passed in parameter is a string and not empty
			if (!!parameter && typeof parameter === 'string' && parameter !== "") {
				response = this._data.getValue(parameter);

			} else {
				this._debug.log(
					"warning",
					{
						type: "Wrapper",
						msg: "Could not fetch data from LMS API Adapter because passed in parameter is not valid."
					}
				);
			}

		} else {
			this._debug = new Debug(null, this._debugLevel);
			this._debug.log(
				"error",
				{
					type: "Wrapper",
					msg: "Could not fetch data from LMS API Adapter because connection is not established."
				}
			);

		}

		return response;
	}

	/** Pushs data to the LMS API Adapter
	 * @param parameter 		String which contains the SCORM data model element
	 * @param value 			String/Number which contains the value to assign to the SCORM data model element
	 * 
	 * @return 					True if value successfull pushed, false if not
	 */
	setValue(parameter, value) {
		let response = false;

		// Check connection is active
		if (this._isConnectionActive) {
			// Check passed in parameter is a string and not empty
			if (
					(!!parameter && typeof parameter === 'string' && parameter !== "") &&
					(!!value && (typeof value === 'string' || typeof value === 'number'))
			) {
				response = this._data.setValue(parameter, value.toString());
	
			} else {
				this._debug.log(
					"warning",
					{
						type: "Wrapper",
						msg: "Could not push data to the LMS API Adapter because passed in parameters are not valid."
					}
				);
			}

		} else {
			this._debug = new Debug(null, this._debugLevel);
			this._debug.log(
				"error",
				{
					type: "Wrapper",
					msg: "Could not push data to the LMS API Adapter because connection is not established."
				}
			);

		}

		return response;
	}

	/** Forces LMS API Adapter to push data to the LMS
	 * 
	 * @return 					True if successfull commited, false if not
	 */
	commit() {
		let response = false;

		// Check connection is active
		if (this._isConnectionActive) {
			response = this._data.commit();
	
		} else {
			this._debug = new Debug(null, this._debugLevel);
			this._debug.log(
				"error",
				{
					type: "Wrapper",
					msg: "Could not force LMS API Adapter to commit data because connection is not established."
				}
			);

		}

		return response;
	}

	/** Fetchs last error code from the LMS API Adapter
	 * 
	 * @return 					Number of the last error, or null if an error occured
	 */
	getLastError() {
		let response = null;

		// Check connection is active
		if (this._isConnectionActive) {
			response = this._debug.getLastErrorCode();
	
		} else {
			this._debug = new Debug(null, this._debugLevel);
			this._debug.log(
				"error",
				{
					type: "Wrapper",
					msg: "Could not fetch error code from the LMS API Adapter because connection is not established."
				}
			);

		}

		return response;
	}

	/** Fetchs error string from the LMS API Adapter
	 * @param errorCode 		Number which contains the error code to obtain a textual description of the error
	 * 
	 * @return 					String of the specified error code, or empty string if an error occured
	 */
	getErrorString(errorCode) {
		let response = "";

		// Check connection is active
		if (this._isConnectionActive) {
			// Check passed in parameter is an empty string or a number
			// TODO: Check input value
			if ( !!errorCode && typeof errorCode === 'number' ) {
				response = this._debug.getErrorString(errorCode);

			} else {
				this._debug.log(
					"warning",
					{
						type: "Wrapper",
						msg: "Could not fetch error string from the LMS API Adapter because passed in parameter is not valid."
					}
				);
			}
	
		} else {
			this._debug = new Debug(null, this._debugLevel);
			this._debug.log(
				"error",
				{
					type: "Wrapper",
					msg: "Could not fetch error string from the LMS API Adapter because connection is not established."
				}
			);

		}

		return response;
	}

	/** Fetchs diagnostic from the LMS API Adapter
	 * @param errorCode 		Number which contains the error code to obtain vendor-specific error descriptions of the error
	 * 							Empty String to obtain vendor-specific error descriptions of the last occurred error
	 * 
	 * @return 					String value of the specified error code, or empty string if an error occured
	 */
	getDiagnostic(errorCode) {
		let response = "";

		// Check connection is active
		if (this._isConnectionActive) {

			// Check passed in parameter is an empty string or a number
			if ( typeof errorCode === 'number' || errorCode === "" ) {
				response = this._debug.getDiagnostic(errorCode);

			} else {
				this._debug.log(
					"warning",
					{
						type: "Wrapper",
						msg: "Could not fetch diagnostic from the LMS API Adapter because passed in parameter is not valid."
					}
				);
			}
	
		} else {
			this._debug = new Debug(null, this._debugLevel);
			this._debug.log(
				"error",
				{
					type: "Wrapper",
					msg: "Could not fetch diagnostic from the LMS API Adapter because connection is not established."
				}
			);

		}

		return response;
	}

}