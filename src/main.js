/**
 * SCORM Class
 *
 * @author 		Christian Merklinger
 * @date 		2020/04/27
 * @version 	1.0.0
 */

// ---------------- Imports ----------------
import Scorm from "./scorm.js";