/**
 * Debug Class
 *
 * @author 		Christian Merklinger
 * @date 		2020/04/27
 * @version 	1.0.0
 */

// ---------------- Imports ----------------
import Connection from "./connection.js";


//------------------------ Debug Class ------------------------

export default class Debug {
	constructor(connection = null, debugLevel = 0) {
		this._connection = connection;
		this._debugLevel = debugLevel;
	}

	set connection(object) {
		if (object instanceof Connection || object === null) {
			this._connection = object;
		}
	}

	get connection() {
		return this._connection;
	}

	set debugLevel(debugLevel) {
		if (typeof debugLevel === "number") {
			this._debugLevel = debugLevel;
		}
	}

	get debugLevel() {
		return this._debugLevel;
	}

	/** Fetchs last error code from the LMS API Adapter
	 * 
	 * @return 					Number of the last error, or null if an error occured
	 */
	getLastErrorCode() {
		let errorCode = null;

		try {
			errorCode = parseInt(this._connection.getAPI().LMSGetLastError(), 10);

		} catch (error) {
			this.log(
				"error",
				{
					type: "Wrapper",
					msg: "An error occured while fetching error code from the LMS API Adapter."
				}
			);
			this.log(
				"error",
				{
					type: "Wrapper",
					msg: error
				}
			);

		}

		return errorCode;
	}

	/** Fetchs error string from the LMS API Adapter
	 * @param errorCode 		Number which contains the error code to obtain a textual description of the error
	 * 
	 * @return 					String of the specified error code, or empty string if an error occured
	 */
	getErrorString(errorCode) {
		let errorString = null;
		
		try {
			errorString = this._connection.getAPI().LMSGetErrorString(errorCode.toString());


		} catch (error) {
			this.log(
				"error",
				{
					type: "Wrapper",
					msg: "An error occured while fetching error string from the LMS API Adapter."
				}
			);
			this.log(
				"error",
				{
					type: "Wrapper",
					msg: error
				}
			);
			
		}

		return errorString;

	}

	/** Fetchs diagnostic from the LMS API Adapter
	 * @param errorCode 		Number which contains the error code to obtain vendor-specific error descriptions of the error
	 * 							Empty String to obtain vendor-specific error descriptions of the last occurred error
	 * 
	 * @return 					String value of the specified error code, or empty string if an error occured
	 */
	getDiagnostic(errorCode) {
		let diagnosticString = null;

		try {
			diagnosticString = String( this._connection.getAPI().LMSGetDiagnostic(errorCode) );


		} catch (error) {
			this.log(
				"error",
				{
					type: "Wrapper",
					msg: "An error occured while fetching diagnostic from the LMS API Adapter."
				}
			);
			this.log(
				"error",
				{
					type: "Wrapper",
					msg: error
				}
			);
			
		}

		return diagnosticString;
	
	}

	/** Logs data to the window console depending on debug level
	 * @param status 			String which contains the status name
	 * @param payload 			Object which contains the log data
	 */
	log(status, payload) {
		if (this._debugLevel === 2) {
			if (status === "error") {
				this.logError(payload);
			} else if (status === "warning") {
				this.logWarning(payload);
			} else if (status === "info") {
				this.logInfo(payload);
			}

		} else if (this._debugLevel === 1) {
			if (status === "error") {
				this.logError(payload);
			} else if (status === "info") {
				this.logInfo(payload);
			}

		} else if (this._debugLevel === 0) {
			if (status === "info") {
				this.logInfo(payload);
			}

		}

	}

	/** Logs info data to the window console
	 * @param payload 			Object which contains the log data
	 *
	 */
	logInfo(payload) {
		if (payload.type === "LMS") {
			console.log(payload.msg);
		} else if (payload.type === "Wrapper") {
			console.log(payload.msg);
		}

	}

	/** Logs warning data to the window console
	 * @param payload 			Object which contains the log data
	 *
	 */
	logWarning(payload) {
		if (payload.type === "LMS") {
			console.warn("Warning: " + payload.msg);
		} else if (payload.type === "Wrapper") {
			console.warn("Warning: " + payload.msg);

		}

	}

	/** Logs error data to the window console
	 * @param payload 			Object which contains the log data
	 *
	 */
	logError(payload) {
		if (payload.type === "LMS") {
			console.error("Error: " + payload.errorString + " (Error Code: " + payload.errorCode + ")");
		} else if (payload.type === "Wrapper") {
			console.error("Error: " + payload.msg);

		}

	}
	
}