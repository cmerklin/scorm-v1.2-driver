/**
 * Connection Class
 *
 * @author 		Christian Merklinger
 * @date 		2020/04/27
 * @version 	1.0.0
 */

// ---------------- Imports ----------------
import Utils from "./utils.js";

//------------------------ Connection Class ------------------------

export default class Connection {
	constructor() {
		this._API = null;
	}

	set API(api) {
		this._API = api;
	}

	get API() {
		return this._API;
	}

	/** Initialize the the connection to the API Adapter
	 * 
	 * @return 					True if connection successfull intialized, false if not
	 */
	initialize() {
		let success = false;
		this._API = this.getAPI();

		if (this._API) {
			if ( Utils.convertStringToBoolean(this._API.LMSInitialize("")) ) {
				success = true;
			}

		}

		return success;

	}

	/** Returns the LMS API Adapter
	 ** If connection is already established it returns the API Adapter reference
	 ** If connection is not established it searches the API Adapter
	 * 
	 * @return 					Reference to the LMS API Adapter if found, NULL if no LMS Adapter found
	 */
	getAPI () {
		if (!this._API) {
			// Find LMS API Adapter in current window
			this._API = this.findAPI(window);

			// Find LMS API Adapter in parent window
			if (!this._API && window.parent && window.parent != window) {
				this._API = this.findAPI(window.parent);
			}

			// Find LMS API Adapter in current window opener
			if (!this._API && window.top && window.top.opener) {
				this._API = this.findAPI(window.top.opener);
			}

		}

		return this._API;
		
	}
	
	/** Looks for an object called API
	 * @param window 			Browser window object
	 *
	 * @return 					API object if found, NULL if no API object found
	 */
	findAPI(window) {
		let API = null;
		let attempts = 0;
		let attemptsLimit = 10;

		while(
			(!window.API) && 
			(window.parent) && 
			(window.parent != window) &&
			(attempts <= attemptsLimit)
		) {
			attempts++

			window = window.parent;
		}

		if (window.API) {
			API = window.API;
		}

		return API;
	}

	/** Terminates the connection with the LMS API Adapter
	 * 
	 * @return 					True if connection successfull terminated, false if not
	 */
	terminate() {
		let success = false;

		if (this._API) {
			if ( Utils.convertStringToBoolean(this._API.LMSFinish("")) ) {
				success = true;
			}

		}

		return success;

	}
	
}